bg = "background.jpg"
ts = "titlescreen.jpg"
parent_one = "parent_one.png"
parent_two = "parent_two.png"
parent_three = "parent_three.png"
parent_four = "parent_four.png"
parent_five = "parent_five.png"

import pygame, sys, random
from pygame.locals import *

pygame.init()

pygame.display.set_caption('Biology H Project')

screen = pygame.display.set_mode((800,600))
ts = pygame.image.load(ts).convert()
bg = pygame.image.load(bg).convert()

clock = pygame.time.Clock()

parent_one = pygame.image.load(parent_one)
parent_two = pygame.image.load(parent_two)
parent_three = pygame.image.load(parent_three)
parent_four = pygame.image.load(parent_four)
parent_five = pygame.image.load(parent_five)

parent_one_poly = (6, 4, 10, 45)
pop_total = 6 + 4 + 10 + 45
parent_two_poly = (14, 14, 22, 22)
ptwp_total = 14 + 14 + 22 + 22
parent_three_poly = (4, 9, 39, 37)
pthp_total = 4 + 9 + 39 + 37
parent_four_poly = (8, 13, 35, 11)
pfop_total = 8 + 13 + 35 + 11
parent_five_poly = (4, 4, 42, 42)
pfip_total = 4 + 4 + 42 + 42

color_one = (165, 114, 18)
co_total = 165 + 114 + 18
color_two = (233, 153, 225)
ctw_total = 233 + 153 + 225
color_three = (129, 180, 216)
cth_total = 129 + 180 + 216
color_four =  (45, 185, 144)
cfo_total = 45 + 185 + 144
color_five = (207, 248, 24)
cfi_total = 207 + 248 + 24

parent_one_total = pop_total + co_total
parent_two_total = ptwp_total + ctw_total
parent_three_total = pthp_total + cth_total
parent_four_total = pfop_total + cfo_total
parent_five_total = pfip_total + cfi_total

#ud_scale = random.randint(0,1)

fight = 0
flight = 0
reproduction = 0

def parentHasCollide(x,y,x_other,y_other):
    if x <= x_other + 50 and y <= y_other + 50 and x >= x_other - 50 and y >= y_other -50:
        return True
    return False
        
def pause():
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                return

x_one,y_one = random.randint(1,50),random.randint(1,50)
x_two,y_two = random.randint(101,150),random.randint(101,150)
x_three,y_three =random.randint(201,250),random.randint(201,250)
x_four,y_four = random.randint(301,350),random.randint(301,350)
x_five,y_five = random.randint(401,450),random.randint(401,450)

speedx_one = random.randint(50,500)
speedx_two = random.randint(50,500)
speedx_three = random.randint(50,500)
speedx_four = random.randint(50,500)
speedx_five = random.randint(50,500)

speedy_one = random.randint(50,500)
speedy_two = random.randint(50,500)
speedy_three = random.randint(50,500)
speedy_four = random.randint(50,500)
speedy_five = random.randint(50,500)

while True:

    screen.blit(bg,(0,0))
    screen.blit(parent_one,(x_one,y_one))
    screen.blit(parent_two,(x_two,y_two))
    screen.blit(parent_three,(x_three,y_three))
    screen.blit(parent_four,(x_four,y_four))
    screen.blit(parent_five,(x_five,y_five))
    
    if x_one <= x_two + 50 and y_one <= y_two + 50 and x_one >= x_two - 50 and y_one >= y_two -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_one_total + parent_two_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")
            reproduction+= 1
    if x_one <= x_three + 50 and y_one <= y_three + 50 and x_one >= x_three - 50 and y_one >= y_three -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_one_total + parent_three_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")
    if x_one <= x_four + 50 and y_one <= y_four + 50 and x_one >= x_four - 50 and y_one >= y_four -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_one_total + parent_four_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")
    if x_one <= x_five + 50 and y_one <= y_five + 50 and x_one >= x_five - 50 and y_one >= y_five -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_one_total + parent_five_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")

    if x_two <= x_three + 50 and y_two <= y_three + 50 and x_two >= x_three - 50 and y_two >= y_three -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_two_total + parent_three_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")         
    if x_two <= x_four + 50 and y_two <= y_four + 50 and x_two >= x_four - 50 and y_two >= y_four -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_two_total + parent_four_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")       
    if x_two <= x_five + 50 and y_two <= y_five + 50 and x_two >= x_five - 50 and y_two >= y_five -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_two_total + parent_five_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")
                
    if x_three <= x_four + 50 and y_three <= y_four + 50 and x_three >= x_four - 50 and y_three >= y_four -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_three_total + parent_four_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")
    if x_three <= x_five + 50 and y_three <= y_five + 50 and x_three >= x_five - 50 and y_three >= y_five -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_three_total + parent_five_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")

    if x_four <= x_five + 50 and y_four <= y_five + 50 and x_four >= x_five - 50 and y_four >= y_five -50:
        rand_fact = random.randint(0,5)
        if rand_fact == 0:
            f_f_r = random.randint(0,2)
        if rand_fact != 0:
            f_f_r = parent_four_total + parent_five_total % 3
        if f_f_r == 0:
            print ("Fight")
            fight+= 1
        if f_f_r == 1:
            print ("Flight")
            flight+= 1
        if f_f_r == 2:
            print ("Reproduciton")
                
    milli=clock.tick()
    seconds=milli/1000.0
    
    dx_one = seconds*speedx_one
    dx_two = seconds*speedx_two
    dx_three = seconds*speedx_three
    dx_four = seconds*speedx_four
    dx_five = seconds*speedx_five
    
    dy_one = seconds*speedy_one
    dy_two = seconds*speedy_two
    dy_three = seconds*speedy_three
    dy_four = seconds*speedy_four
    dy_five = seconds*speedy_five

    y_one+=dy_one
    y_two+=dy_two
    y_three+=dy_three
    y_four+=dy_four
    y_five+=dy_five
    
    x_one+=dx_one
    x_two+=dx_two
    x_three+=dx_three
    x_four+=dx_four
    x_five+=dx_five

    if x_one > 800:
        speedx_one = speedx_one * -1
    if y_one > 600:
        speedy_one = speedy_one * -1
    if x_one < 0:
        speedx_one = speedx_one * -1
    if y_one < 0:
        speedy_one = speedy_one * -1
        
    if x_two > 800:
        speedx_two = speedx_two * -1
    if y_two > 600:
        speedy_two = speedy_two * -1
    if x_two < 0:
        speedx_two = speedx_two * -1
    if y_two < 0:
        speedy_two = speedy_two * -1
        
    if x_three > 800:
        speedx_three = speedx_three * -1
    if y_three > 600:
        speedy_three = speedy_three * -1
    if x_three < 0:
        speedx_three = speedx_three * -1
    if y_three < 0:
        speedy_three = speedy_three * -1
        
    if x_four > 800:
        speedx_four = speedx_four * -1
    if y_four > 600:
        speedy_four = speedy_four * -1
    if x_four < 0:
        speedx_four = speedx_four * -1
    if y_four < 0:
        speedy_four = speedy_four * -1
        
    if x_five > 800:
        speedx_five = speedx_five * -1
    if y_five > 600:
        speedy_five = speedy_five * -1
    if x_five < 0:
        speedx_five = speedx_five * -1
    if y_five < 0:
        speedy_five = speedy_five * -1

    for event in pygame.event.get():
        if event.type == KEYDOWN:
            print("Fight: ",fight,"Flight: ", flight, "Reproduction: ", reproduction)
            print("Parent_one_speed",speedx_one," ",speedy_one,
                  "Parent_two_speed",speedx_two," ",speedy_two,
                  "Parent_three_speed",speedx_three," ",speedy_three,
                  "Parent_four_speed",speedx_four," ",speedy_four,
                  "Parent_five_speed",speedx_five," ",speedy_five)
            pause()
        
    pygame.display.update()
