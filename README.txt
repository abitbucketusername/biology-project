Note: Uses the Pygame module

Note: Code is left as is from point of creation with the same data used in the final simulation,
the final simulation meaning the simulation used in my presentation.
Note: Code is almost entirely uncommented and lacks explantion/readability

Important Information-

The visual representation is only a representation of the caculations used and are not
directly representive of anything and can almost be left out entirly, expect for cases of death.
Death is when a population leaves the environment and doesn't come back during anypoint
of the simulation.
+ is cool to look at

Data generated from Main_Generate.py has to be entered manually into Main_Test.py
But this is not needed to run the simulation because it is using the information from
the final simulation.

Data-Storage contains all the data used in the final simulation

Results.txt contains the results from the final simulation

Press "Esc" to end simulation, simulation can encounter much bad otherwise or
cause become inaccuracys

Description -

The project generates Parent Data, data meant to represent its
respective population, the population derived from the Parent data 
rather than pre-existing, data wise. It does this from Main_Generate.py
from here it will create visual representations of the
populations/parent-representations of the population then it will print 
out information to the console. This information represents the data
to be used in the main simulation. This information has to be entered manually into
Main_Test.py ,but is not required because the file still contains the data used in the final
simulation. After you run the simulation the program will start and run until you press
the "Esc" key then it will print the results to the console. These are the results
of population interaction and then velocity varabiles, called speed which is contained seperately but also displays direction information. The velocity varabiles are random and
used in chance to interact calculations. Not meant to be looked at as movement ,but
as a varabile in an algorithm for randomness.
