bg = "background.jpg"
tspg = "titlescreen_parent_generator.jpg"
save_one = "parent_one.png"
save_two = "parent_two.png"
save_three = "parent_three.png"
save_four = "parent_four.png"
save_five = "parent_five.png"

import pygame, sys, random
from pygame.locals import *

pygame.init()

pygame.display.set_caption('Biology H Project')

screen = pygame.display.set_mode((800,600))
save_screen = pygame.display.set_mode((50,50))
bg = pygame.image.load(bg).convert()
tspg = pygame.image.load(tspg).convert()

def pause():
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                return;

parent_one_circle = 0
parent_two_circle = 0
parent_three_circle = 0
parent_four_circle = 0
parent_five_circle = 0

position = (25,25)

radius = random.randint(1,25)

points = []
null_points = [(0,0),(0,0),(0,0)]

color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

size_of_parent = random.randint(0,3)

points_three = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_four = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_five = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

if (size_of_parent == 0):

    points=points_three
    
if (size_of_parent == 1):
    
    points=points_four
    
if (size_of_parent == 2):
    
    points = points_five

if (size_of_parent == 3):

    points = null_points

color_one = color

parent_one = pygame.draw.polygon(screen, color, points)

if (size_of_parent == 3):
    parent_one_circle = 1
    parent_one = pygame.draw.circle(screen, color, position, radius)

pygame.image.save(save_screen,save_one)
screen.blit(bg,(0,0))

radius = random.randint(1,25)

color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

size_of_parent = random.randint(0,3)

points_three = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_four = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_five = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

if (size_of_parent == 0):

    points = points_three
    
if (size_of_parent == 1):
    
    points = points_four
    
if (size_of_parent == 2):
    
    points = points_five

if (size_of_parent == 3):

    points = null_points

color_two = color

parent_two = pygame.draw.polygon(screen, color, points)

if (size_of_parent == 3):
    parent_two_circle = 1
    parent_two = pygame.draw.circle(screen, color, position, radius)

pygame.image.save(save_screen,save_two)
screen.blit(bg, (0,0))

radius = random.randint(1,25)

color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

size_of_parent = random.randint(0,3)

points_three = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_four = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_five = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

if (size_of_parent == 0):

    points = points_three
    
if (size_of_parent == 1):
    
    points = points_four
    
if (size_of_parent == 2):
    
    points = points_five
    
if (size_of_parent == 3):

    points = null_points
    
color_three = color

parent_three = pygame.draw.polygon(screen, color, points)

if (size_of_parent == 3):
    parent_three_circle = 1
    parent_three = pygame.draw.circle(screen, color, position, radius)

pygame.image.save(save_screen,save_three)
screen.blit(bg,(0,0))

radius = random.randint(1,25)

color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

size_of_parent = random.randint(0,3)

points_three = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_four = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_five = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

if (size_of_parent == 0):

    points = points_three
    
if (size_of_parent == 1):
    
    points = points_four
    
if (size_of_parent == 2):
    
    points = points_five
    
if (size_of_parent == 3):

    points = null_points

color_four = color

parent_four = pygame.draw.polygon(screen, color, points)

if (size_of_parent == 3):
    parent_four_circle = 1
    parent_four = pygame.draw.circle(screen, color, position, radius)

pygame.image.save(save_screen,save_four)
screen.blit(bg,(0,0))

radius = random.randint(1,25)

color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

size_of_parent = random.randint(0,3)

points_three = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_four = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

points_five = [(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50)) ,
(random.randint(1,50),random.randint(1,50))]

if (size_of_parent == 0):

    points=points_three
    
if (size_of_parent == 1):
    
    points=points_four
    
if (size_of_parent == 2):
    
    points=points_five
    
if (size_of_parent == 3):
    
    points = null_points

color_five = color

parent_five = pygame.draw.polygon(screen, color, points)

if (size_of_parent == 3):
    parent_five_circle = 1
    parent_five = pygame.draw.circle(screen, color, position, radius)

pygame.image.save(save_screen,save_five)
screen.blit(bg,(0,0))
if (parent_one_circle == 1):
    print ("Parent_One_is_Cirlce ")
    
print ("Parent_One " , parent_one , "Parent_One_Color" , color_one , "\n")

if (parent_two_circle == 1):
    print ("Parent_Two_is_Circle ")
    
print ("Parent_Two" , parent_two , "Parent_Two_Color" , color_two , "\n")

if (parent_three_circle == 1):
    print ("Parent_Three_is_Circle ")
    
print ("Parent_Three" , parent_three , "Parent_Three_Color" , color_three , "\n")

if (parent_four_circle == 1):
    print ("Parent_Four_is_Cirlce ")
    
print ("Parent_Four" , parent_four, "Parent_Four_Color" , color_four , "\n")

if (parent_five_circle == 1):
    print ("Parent_Five_is_Circle ")
    
print ("Parent_Five" , parent_five, "Parent_Five_Color" , color_five)

screen = pygame.display.set_mode((800,600))

while True:

    screen.blit(tspg,(0,0))
    pygame.display.update()
    pause()
    
